from flask import Flask, render_template
import os

app = Flask(__name__);

@app.route('/')
@app.route('/home')
@app.route('/home.html')
def home():
    return render_template('home.html')


@app.route('/News')
@app.route('/News.html')
def news():
    return render_template('News.html')

@app.route('/News2')
@app.route('/News2.html')
def news2():
    return render_template('News2.html')
    
@app.route('/News3')
@app.route('/News3.html')
def news3():
    return render_template('News3.html')
    
@app.route('/News4')
@app.route('/News4.html')
def news4():
    return render_template('News4.html')
    
@app.route('/Social')
@app.route('/Social.html')
def Social():
    return render_template('Social.html')
    
@app.route('/UFCgame')
@app.route('/UFCgame.html')
def UFCgame():
    return render_template('UFCgame.html')
    
@app.route('/upcomingEvents')
@app.route('/upcomingEvents.html')
def upcomingEvents():
    return render_template('upcomingEvents.html')
    
if __name__ == '__main__':
    app.run(port=8080, host='0.0.0.0', debug=True)
    
    
    