//This function is used to control to rollover image
//so when the cursor is on the image it will chnage state

function MouseRollover(MyImage) {
    MyImage.src = "static/imgAssets/Rollover2.jpg";
}
function MouseOut(MyImage) {
    MyImage.src = "static/imgAssets/Rollover1.jpg";
}

//this function chnages the image state on click
function changeImage() {
    var image = document.getElementById('id1');
    if (image.src.match("2")) {
        image.src = "static/imgAssets/Main.jpg";
    } else {
        image.src = "static/imgAssets/Main2.jpg";
    }
}
//this function shows the visability of an image at will through buttons

function hideElem() {
    document.getElementById("myImg").style.visibility = "hidden"; 
}

function showElem() {
    document.getElementById("myImg").style.visibility = "visible"; 
}

//This funnction uses Jquery but improves design by adding transition on loading

$(window).scroll(function() {
  $(".slideanim").each(function(){
    var pos = $(this).offset().top;

    var winTop = $(window).scrollTop();
    if (pos < winTop + 600) {
      $(this).addClass("slide");
    }
  });
});

//the functions below retrieves the tags which preforms events to
//show and hide data on the website for each article

var demo = document.getElementById('demo');
var para = document.getElementById('para');
var butt = document.getElementById('close');

var demo1 = document.getElementById('demo1');
var para1 = document.getElementById('para1');
var butt1 = document.getElementById('close1');

var demo2 = document.getElementById('demo2');
var para2 = document.getElementById('para2');
var butt2 = document.getElementById('close2');

var demo3 = document.getElementById('demo3');
var para3 = document.getElementById('para3');
var butt3 = document.getElementById('close3');

demo.onclick = function() {
    display(para, butt);
};

butt.onclick = function() {
    close(para, butt);
};

demo1.onclick = function() {
    display(para1, butt1);
};

butt1.onclick = function() {
    close(para1, butt1);
};

demo2.onclick = function() {
    display(para2, butt2);
};

butt2.onclick = function() {
    close(para2, butt2);
};

demo3.onclick = function() {
    display(para3, butt3);
};

butt3.onclick = function() {
    close(para3, butt3);
};


function close(paragraph, button) {
    paragraph.style.display = "none";
    button.style.display = "none";
}

function display(paragraph, button){
    paragraph.style.display = "block";
    button.style.display = "block";
}

